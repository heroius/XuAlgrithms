﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.InterpolationApproximation
{
    public class CHIR:SampleItem
    {
        public CHIR() : base("InterpolationApproximation.CHIR") { }

        public override string Execute()
        {
            double[] x = new double[101], y = new double[101], a = new double[7];
            for (int i = 0; i < 101; i++)
            {
                x[i] = -1.0 + 0.02 * i;
                y[i] = Math.Atan(x[i]);
            }
            Heroius.XuAlgrithms.InterpolationApproximation.CHIR(x, y, 101, out a, 6);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 6; i++)
            {
                builder.AppendLine($"a{i} = {a[i]}");
            }
            builder.AppendLine($"MAX(p-f)={a[6]}");
            return builder.ToString();
        }
    }
}
