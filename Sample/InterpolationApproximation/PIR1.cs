﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.InterpolationApproximation
{
    public class PIR1: SampleItem
    {
        public PIR1() : base("InterpolationApproximation.PIR1") { }

        public override string Execute()
        {
            double[] x = new double[20], y = new double[20], a = new double[6], dt = new double[3];
            for (int i = 0; i < 20; i++)
            {
                x[i] = 0.1 * i;
                y[i] = x[i] - Math.Exp(-x[i]);
            }
            Heroius.XuAlgrithms.InterpolationApproximation.PIR1(x, y, 20, out a, 6, out dt);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 6; i++)
            {
                builder.AppendLine($"a{i}={a[i]}");
            }
            for (int i = 0; i < 3; i++)
            {
                builder.AppendLine($"dt{i}={dt[i]}");
            }
            return builder.ToString();
        }
    }
}
